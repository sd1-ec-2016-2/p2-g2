var express = require('express');  // módulo express
var app = express();		   // objeto express
var bodyParser = require('body-parser');  // processa corpo de requests
var cookieParser = require('cookie-parser');  // processa cookies
var irc = require('irc');
var path = require('path'); //permite o envio de arquivos estáticos para os clientes

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded( { extended: true } ));
app.use(cookieParser());
app.use(express.static('public'));

var path = require('path');	// módulo usado para lidar com caminhos de arquivos

var proxies = {}; // mapa de proxys
var proxy_id = 0;
var lista_canais = [];
var lista_usuarios = [];
var comecou;
function proxy(id, servidor, nick, canal) {
	var cache = []; // cache de mensagens

	var irc_client = new irc.Client(
			servidor,
			nick,
			{channels: [canal],});

	irc_client.addListener('message'+canal, function (from, message) {
	    console.log(from + ' => '+ canal +': ' + message);
	    cache.push(	{"timestamp":Date.now(),
			"nick":from,
			"msg":message} );
	});
	irc_client.addListener('channellist', function(channel_list) {
				for (var i=0; i<10; i++){
					lista_canais.push(channel_list[i]);
				}
				terminou = true;
		});
			irc_client.addListener('join', 	function (channel, nick, message) {
				console.log(nick +' entrou no canal: '+ channel);
			});
		irc_client.addListener('nick', function(oldnick, newnick, channels, message) {
			console.log('NICK: ' + oldnick + ' mudou para ' + newnick);
		});
		irc_client.addListener('names', function(channel,nicks) {
			console.log("Começou a lista");
			for(var i =0; i<Object.keys(nicks).length; i++){
				lista_usuarios.push(Object.keys(nicks)[i]);
			}

			});
	irc_client.addListener('error', function(message) {
	    console.log('error: ', message);
	});
	proxies[id] = { "cache":cache, "irc_client":irc_client  };

	return proxies[id];
}

app.get('/', function (req, res) {
  if ( req.cookies.servidor && req.cookies.nick  && req.cookies.canal ) {
	proxy_id++;
	var p =	proxy(	proxy_id,
			req.cookies.servidor,
			req.cookies.nick,
			req.cookies.canal);
	res.cookie('id', proxy_id);
  	res.sendFile(path.join(__dirname, '/index.html'));
  }
  else {
        res.sendFile(path.join(__dirname, '/login.html'));
  }
});

app.get('/obter_mensagem/:timestamp', function (req, res) {
  var id = req.cookies.id;
  res.append('Content-type', 'application/json');
  res.send(proxies[id].cache);
});

app.post('/gravar_mensagem', function (req, res) {
  proxies[req.cookies.id].cache.push(req.body);
  var irc_client = proxies[req.cookies.id].irc_client;
  irc_client.say(irc_client.opt.channels[0], req.body.msg );
  res.end();
});

app.post('/login', function (req, res) {
   res.cookie('nick', req.body.nome);
   res.cookie('canal', req.body.canal);
   res.cookie('servidor', req.body.servidor);
   res.redirect('/');
});

app.get('/comando/lista_canais',function(req,res){
	var irc_client = proxies[req.cookies.id].irc_client;
	irc_client.list(req.cookies.servidor);
	terminou = false;
	res.send(comecou);
});

app.get('/comando/mudar_nick/:args',function(req,res){
	var arg = req.params.args;
	var irc_client = proxies[req.cookies.id].irc_client;
	irc_client.send("nick", arg);
	irc_client.emit("nick", req.cookies.nick, arg, req.cookies.canal);
	res.send(arg);
});

app.get('/comando/entrar_canal/:args',function(req,res){
	var arg = req.params.args;
	var irc_client = proxies[req.cookies.id].irc_client;
	arg = "#" + arg;
	irc_client.join(arg);
	irc_client.send("join", arg);
	irc_client.emit("join", arg, req.cookies.nick);
	var canal_atual = irc_client.opt.channels[0];
	irc_client.opt.channels[0] = arg;
	irc_client.opt.channels.push(canal_atual);
	res.send(arg);
});

app.get('/obter_list_usuario', function(req,res){
	var irc_client = proxies[req.cookies.id].irc_client;
	var canal = req.cookies.canal;
	irc_client.send('names', canal);
	res.send(lista_usuarios);
});

app.get('/obter_lista', function(req,res){
	if(!terminou){
		res.send(terminou)
	}else {
		res.send(lista_canais);
	}

});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
